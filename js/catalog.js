$(document).ready(function(){

// Get our cookies
var cookies = {
	left_panel: $.cookie('left_panel'), 
	right_panel: $.cookie('right_panel')
};

// Checking cookies
for (var key in cookies) {
	if (cookies[key] == 1){
		var cookie_marker = 1;
		mini_panel(key, cookie_marker);
	} else {}
}

// Add new cookie with panel status (1 - hide)
function add_cookie(element){
	$.cookie(element, '1');
}

// Mini-panel (when we hide panel we get mini-panel)
function mini_panel(panel, cookie_marker){
	
	if (cookie_marker === 1 && key == 'left_panel'){
		$('.' + panel).css('left', '0');
		$('section').css('margin-left', '40px');
		$('.' + panel).addClass('mini_panel');
	} else if (cookie_marker === 1 && key == 'right_panel'){
		$('.' + panel).css('right', '0');
		$('section').css('margin-right', '40px');
		$('.' + panel).addClass('mini_panel');

	} else if (panel === 'left_panel'){
		$('.' + panel).stop().animate({left: '-100px'}, 200, function(){
			$('.' + panel).addClass('mini_panel');
			add_cookie(panel);
		});
	} else if (panel === 'right_panel'){
		$('.' + panel).stop().animate({right: '-100px'}, 200, function(){
			$('.' + panel).addClass('mini_panel');
			add_cookie(panel);
		});
	}
}

// Restore panel from mini to normal
function restore_panel(panel) {

	if (panel == 'left_panel'){
		$('.' + panel).stop().animate({'left': '-30px'}, 200, function(){
			$('.left_panel').css('left', '-100px');
			$('.' + panel).removeClass('mini_panel');
			$('.left_panel').stop().animate({'left':'0px'},200);
			$('section').stop().animate({'margin-left': '110px'}, 200);
		});
	} else {
			$('.' + panel).stop().animate({'right': '-30px'}, 200, function(){
			$('.right_panel').css('right', '-100px');
			$('.' + panel).removeClass('mini_panel');
			$('.right_panel').stop().animate({'right':'0px'},200);
			$('section').stop().animate({'margin-right': '110px'}, 200);
		});
	}
$.cookie(panel, null);
}

// Hide our panels with simple jQuery animation
function hide_panel(panel){
	if (panel == 'left_panel'){
		$('.' + panel).stop().animate({left: '0px'}, 200, mini_panel(panel));
		$('section').stop().animate({'margin-left':'40px'}, 200);
	} else {
		$('.' + panel).stop().animate({right: '0px'}, 200, mini_panel(panel));
		$('section').stop().animate({'margin-right':'40px'}, 200);
	}
}

// Status our navigation panels, tech function for keyboard shortcuts
function panels_status(panel){
	if ($.cookie(panel) == 1){
		restore_panel(panel);
	} else {
		hide_panel(panel);
	}
}

// Keyboard function
function keyboard_controller(k){

	switch (k.which){
		case 65: 
			panels_status('left_panel');
			break
		case 68:
			panels_status('right_panel');
			break
		case 83:
			$('.search_input').focus();
			break
	}
}

// Click event on panels (minimization panel)
$('.hide_panel').on('click', function(){
	var panel = $(this).parent().attr('class');
		hide_panel(panel);
});

// Click event for restoring panel
$('.maxi_panel').on('click', function(){
	var panel = $(this).parent().attr('class').split(' ')[0]; // take only first class
		restore_panel(panel);
});

// Scroll event for categories in both panels (lections, books, matherials, my, etc)
$(window).on('scroll', function(){
	var y_scroll = $(window).scrollTop();
	if (y_scroll > 60) {
		$('nav > div').stop().animate({'padding-top':'20px'}, 200);
	} else {
		$('nav > div').stop().animate({'padding-top':'100px'}, 200);
	}
});

// Keyboard event for panels (hide/show panels)
$(window).on('keydown', function(k){

	$('.search_input').focusin(function(){
		console.log('Wow!');
	});

	keyboard_controller(k);

});

});